let pushupsDone = 0;

function doPushup() {
    // Recursive functions MUST have a condition
    if (pushupsDone < 50) {
        // Increment the counter
        pushupsDone++;
        console.log('Doing pushup number: ' + pushupsDone);
        // Call function recursively
        doPushup();
    }

    return 'Nice Work!';
}

// Start the function
doPushup();