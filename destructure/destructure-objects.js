const project = {
    name: 'Project Pineapple',
    dateStart: '2020-02-26',
    dateDue: '2020-04-25',
    participants: {
        count: 4,
        team: ['ewoks', 'anikin', 'han', 'luke']
    },
    tasks: {
        count: 0,
        items: []
    }
}

// Destructure items from Object.
const { name, dateStart } = project;
const { team } = project.participants
const { items } = project.tasks;

console.log(name);
console.log(dateStart);
console.log(team);
console.log(items);


// Assign to new variable names
const obj = { a: 123, b: 4234, c: '1233' };
const { a: total, b: max } = obj;


// Set default values
// If port does not exist on process.env it is set to 3000.
const { port = 3000 } = process.env;

// More examples: 
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment#Object_destructuring