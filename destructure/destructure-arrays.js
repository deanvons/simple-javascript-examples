// Get values from array.
const colours = ['#e23131', '#316fe2', '#31e29e', '#dfe231'];
const [red, blue, green, yellow] = colours;

console.log(red); // -> #e23131
console.log(blue); // -> #316fe2
console.log(green); // -> #31e29e
console.log(yellow); // -> #dfe231

// Assign seperate from declaration
let username, email;
[username, email] = ['dewaldels', 'dewaldifels@gmail.com'];

console.log(username); // -> dewaldels
console.log(email); // -> dewaldifels@gmail.com

// Swapping variables
[username, email] = [email, username];
console.log(username); // -> dewaldifels@gmail.com
console.log(email); // -> dewaldels

// More reading here:
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment#Array_destructuring