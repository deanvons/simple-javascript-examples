// Do while is a post test. Even if the battery was 100%
// the first iteration of the loop would still execute.
let phoneBatteryPerc = 0; // 0 % charged.

do {
    // Charge phone
    phoneBatteryPerc += 0.5;
    console.log('Battery charged: ' + phoneBatteryPerc + '%');
} while (phoneBatteryPerc < 100);