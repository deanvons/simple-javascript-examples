// Create a new object to iterate through it's properties.
const employee = {
    id: 1,
    name: 'Dewald',
    surname: 'Els',
    email: 'dewaldifels@gmail.com',
    age: 38,
    jobTitle: 'Teacher'
};

// The for in loop will iterate through the keys of an object.
for (const key in employee) {
    console.log('Current object key/property: ', key);
    console.log('Key Value: ', employee[key]);
    console.log('-----------------------------------------');
}