// Create a new object to iterate through it's properties.
const array = [1, 2, 3, 4, 5, 6, 7, 8];

// The for of loop will iterate through the values of an array (NOT availabe on an Object).
for (const val of array) {
    console.log('Current array value: ', val);
    console.log('-----------------------------------------');
}

/**
For comparison: for-in vs for-of
 
const arr = [3, 5, 7];
arr.foo = 'hello';

for (let i in arr) {
   console.log(i); // logs "0", "1", "2", "foo"
}

for (let i of arr) {
   console.log(i); // logs 3, 5, 7
}

*/