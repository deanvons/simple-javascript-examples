// For loop with a preset number (10)
for (let i = 0; i < 10; i++) {
    console.log('Loop iteration number: ' + i);
}

// For loop based on length of an array
const todoList = ['Buy milk', 'Learn about Loops', 'Run a marathon', 'Sleep', 'Eat some snacks'];

for (let i = 0; i < todoList.length; i++) {
    console.log('On my Todo list is: ' + todoList[i]);
}