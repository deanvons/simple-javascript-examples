// --Merging objects--
// Define two people objects
const personInfo1 = { name: "Alice", age: 30, occupation: "Engineer" };
const personInfo2 = { age: 32, occupation: "Designer", city: "New York" };

// Use spread operator to merge person1 and person2
const mergedPersonInfo = { ...personInfo1, ...personInfo2 };

// The mergedPerson will have properties from both person1 and person2.
// In case of overlapping properties, the last one (person2 in this case) will take precedence.
console.log(mergedPersonInfo); 
// Output: { name: "Alice", age: 32, occupation: "Designer", city: "New York" }

// --Cloning objects--
// Define a person object
const person = { name: "Bob", age: 25, occupation: "Artist" };

// Cloning the person object using the spread operator
const clonedPerson = { ...person };

// The clonedPerson is a shallow copy of person
console.log(clonedPerson); 
// Output: { name: "Bob", age: 25, occupation: "Artist" }

// --Updating objects--

// Define an initial person object
const initialPerson1 = { name: "Charlie", age: 40, occupation: "Teacher" };

// Suppose we want to update the 'age' and 'occupation' of this person
// Use spread operator to create a new object with the updated properties
const updatedPerson1 = { ...initialPerson1, age:45,occupation:"Principal" };

console.log(updatedPerson1); 
// Output: { name: "Charlie", age: 45, occupation: "Principal" }

// Another approach
// Define an initial person object
const initialPerson2 = { name: "Charlie", age: 40, occupation: "Teacher" };

// Suppose we want to update the 'age' and 'occupation' of this person
const updates = { age: 45, occupation: "Principal" };

// Use spread operator to create a new object with the updated properties
const updatedPerson2 = { ...initialPerson2, ...updates };

console.log(updatedPerson2); 
// Output: { name: "Charlie", age: 45, occupation: "Principal" }


