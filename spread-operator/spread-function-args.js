// Function accepts 3 arguments.
function sum(x, y, z) {
    return x + y + z;
}
// Array we want to sum
const numbers = [1, 2, 3];
// Use spread operator to easily pass
const result = sum(...numbers);

console.log(result); // -> 6
