// Greeter Factory.
function greeterFactory(greeting = "Hello", name = "World", punctuation = "!") {
    return {
        greet: function () {
            return `${greeting}, ${name}${punctuation}`;
        }
    }
};

console.log(greeterFactory().greet());

// Unhappy Factory.
const unhappyGreeter = function (greeter) {
    return function (greeting, name) {
        return greeter(greeting, name, " 😭");
    }
}
console.log(unhappyGreeter(greeterFactory)("Hello", "everyone").greet());

// Enthusiastic Factory.
const enthusiasticGreeter = function (greeter) {
    return function (greeting, name) {
        return greeter(greeting.toUpperCase(), name.toUpperCase(), " 😁");
    }
}
console.log(enthusiasticGreeter(greeterFactory)("Helloooo", "everyone").greet());

// Angry Factory
const angryGreeter = function(greeter) {
    return function(greeting, name) {
        return greeter(greeting, name, " 😡");
    }
}

console.log(angryGreeter(greeterFactory)("I'm so mad at you", "Barney").greet())

// Aggresive Factory
const aggresiveGreeter = enthusiasticGreeter(angryGreeter(greeterFactory));
console.log(aggresiveGreeter("You are late", "Jim").greet());
