// Talker factory
const talker = (state) => ({
    talk: () => console.log(state.sound + ', I am ' + state.name)
});

// Diver factory
const driver = (state) => ({
    drive: () => state.position = state.position + state.speed
});

// Killer factory
const killer = (state) => ({
    kill: () => console.log(state.name + 'is busy Killing!')
});

// Pooper factory
const pooper = (state) => ({
    poop: () => console.log(state.name + ' just pooped...')
})

const flyer = (state) => ({
    fly: ()=> console.log(state.name + ' is now flying...')
});

// Dog factory
const Dog = (name) => {
    let state = {
        name,
        sound: 'Woof!'
    }

    return Object.assign(
        {},
        talker(state),
        pooper(state)
    )
};

// Squirrel Factory
const Squirrel = (name) => {
    let state = {
        name,
        sound: 'Squeak'
    };

    return Object.assign(
        {},
        talker(state),
        pooper(state),
        flyer(state)
    );
}

// Cat factory
const Cat = (name) => {
    let state = {
        name,
        sound: 'Meow'
    };

    return Object.assign(
        {},
        talker(state),
        pooper(state)
    );
}

// Creepy Dog Robot factory
const MurderRobotDog = (name) => {
    let state = {
        name,
        speed: 1.5,
        position: 0,
        sound: 'Woof'
    };

    return Object.assign(
        {},
        talker(state),
        driver(state),
        killer(state),
        flyer(state)
    );
}

const roger = Dog('Roger');
console.log(roger.talk()); // Woof, I am Roger
console.log(roger.poop()); // Roger just pooped...

const nancy = Cat('Nancy');
console.log(nancy.talk()); // Meow, I am Nancy
console.log(nancy.poop()); // Nancy just pooped

const bob = Squirrel();
bob.fly();

const roboDog = MurderRobotDog('RoboDOG');
console.log(roboDog.talk()); // Woof, I am RoboDOG
console.log(roboDog.poop()); // Uncaught, TypeError: poop is not a function 👏 Nice, no unused code here!