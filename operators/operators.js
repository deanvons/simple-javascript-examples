let number = 42;
number = number + 2;
//This is equivalent to number = number + 2;
number += 2;

let number2 = 42;
number2 = number2 - 2;
//This is equivalent to number = number - 2;
number2 -= 2;