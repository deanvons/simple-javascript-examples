// Get an element with the id of my-image
const image = document.getElementById('my-image');
// Change the image source


// Get ALL the elements with the class .list-item
const elListItems = document.querySelectorAll('.list-item');
console.log(elListItems);

// Attach an EventListener to a button
const elBtn = document.getElementById('my-btn');
elBtn.addEventListener('click',  function(){
    // this will refer to the button being clicked
    image.src = 'https://jenniferjonesblog.files.wordpress.com/2015/03/vaughn-stock-3.jpg?w=300&h=200';
})

// Attach an EventListener to an input
const elName = document.getElementById('input-name');
elName.addEventListener('keyup', function() {
    // Log the value of the input when a key is released.
    console.log(this.value);
});