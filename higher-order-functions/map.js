const newDevelopers = ['Berra', 'Jonah', 'Berra', 'Alex', 'Esbjorn', 'Saman', 'Oliver', 'Christian', 'Robert', 'Tina', 'Levi', 'Mihales'];

console.log('\nNew Developer Names:\n', JSON.stringify(newDevelopers));

// Map can "Convert" one array to another type of array wihtout modifying the original array
// employeeRecords is based off of newDevelopers but it is completely different.
const employeeRecords = newDevelopers.map((developerName, index)=>{
    return {
        employeeId: index,
        jobTitle: 'JavaScript Developer',
        name: developerName,
        company: 'Scania'
    };
});

console.log('\nEmployee Records:');
console.log(JSON.stringify(employeeRecords));