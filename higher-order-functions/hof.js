function hof(callback) {
  console.log("Hello from hof");
  callback();
}

function callbackOne() {
  console.log("Hello from callback one");
}

function callbackTwo() {
  console.log("Hello from callback two");
}

hof(callbackOne);
hof(callbackTwo);
hof(() => console.log("Hello from anonymous function"));

function count(array, logic) {
  let counter = 0;
  for (let number of array) {
    if (logic(number)) {
      counter++;
    }
  }
  return counter;
}

function isEven(number) {
  if (number % 2 == 0) {
    return true;
  } else {
    return false;
  }
}

function isDoubleDigit(number) {
  if (number > 9 && number < 100) {
    return true;
  } else {
    return false;
  }
}

let numbers = [1,2,3,4,5,6,7,8,9,10,11,12,13];

console.log(count(numbers, isEven));
console.log(count(numbers, isDoubleDigit));
console.log(count(numbers, (number) => number > 8));
