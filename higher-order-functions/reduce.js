// Sales done for the day
const sales = [100, 205, 343, 3344, 25, 456, 37];
// Starting balance of cashier
const cashierFloat = 500;

// Use reduce to "reduce" the values to a total. The -cashierFloat is the initial value of total (- is added to make it negative)

// Old school way 😴
const total = sales.reduce(function (currentTotal, currentSale) {
    return currentTotal + currentSale;
}, -cashierFloat);

// Using Arrow function 😎
const total2 = sales.reduce((currentTotal, currentSale) => currentTotal + currentSale, -cashierFloat);

console.log('Total sales: ' + total);



//// Reverse an array without the .reverse function
function reverse(array) {
    return array.reduce(function (accumulator, currentValue, i) {
        accumulator[i] = array[(array.length - 1) - i];
        return accumulator;
    }, []);
}