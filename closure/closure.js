// Closures help us build reusable functions with similar functionality but slight variations.
const multiplier = function(number1) {
    // Return a function.. inside a function.
    return function(number2) {
        // number1 is stored in this execution context
        return number1 * number2;
    }
}

// Build a funtion that can multiply by 2
const timesTwo = multiplier(2);
const ten      = timesTwo(5);

// Build a function that can multiply by 5
const timesFive = multiplier(5);
const twenty = timesFive(4);

// another way to look at closures showing how it hides values
const secretMessageDisplayer = function() {
    let secret = "Super secret message: JS is weird..wat"
    return function() {
        // number1 is stored in this execution context
        console.log(`This function can access a secret message ${secret}`)
    }
}

let display = secretMessageDisplayer();

// no way to access the variable declared in line 20, only the function extracted in line 27 has access to it, it is 'hidden'
display();