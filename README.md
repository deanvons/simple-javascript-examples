# Vanilla JSFundamentals

Intended for learning basic JS features

## Getting Started

Move .js files to Web project and embed in HTML

OR

Run in node

node <filename>.js

### Prerequisites

Any JS runtime environment

## Authors

***Dean von Schoultz** [deanvons](https://gitlab.com/deanvons)




