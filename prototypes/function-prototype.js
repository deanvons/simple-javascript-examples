function Employee(empId, name, email) {
    this.employeeId = empId;
    this.name = name;
    this.email = email;
}

// Add shared functionality to the Employee prototype
// This will ensure that the function is not added to every instance of Employee
// Rather, a reference is attached to the Employee prototype -> Less memory! Yay! 🕺
Employee.prototype.setJobTitle = function(jobTitle) {
    this.jobTitle = jobTitle;
}

const dewald = new Employee(1, 'Dewald', 'dewaldifels@gmail.com');
console.log(dewald); // -> { employeeId: 1, name: "Dewald", email: "dewaldifels@gmail.com" }

dewald.setJobTitle('Developer');
console.log(dewald); // -> { employeeId: 1, name: "Dewald", email: "dewaldifels@gmail.com", jobTitle: "Developer" }