// Function that accepts a callback
function getMessage(completed) {
    // Take some time to build a message...
    console.log('Building message...'); // -> FIRST LOG

    setTimeout(function () { // wait for 2 seconds
        // Once the timeout is done, send the message...
        completed('Hello from the callback!');
    }, 2000);
}

getMessage((message)=>{
    console.log('Got the message: ' + message); // -> LAST LOG
});

console.log('Im going to do some stuff in the mean time...');  // -> SECOND LOG
console.log('Oh wait! me too, i have something to do...'); // THIRD LOG