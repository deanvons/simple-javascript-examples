// Assign a function to variable
const workHard = function(subject) {
    console.log('Working hard for ' + subject);
}

workHard();

// Functions expression are NOT hoisted.

// Using arrow function
const sleep = () => {
    console.log('🐑🐑🐑');
}

sleep();