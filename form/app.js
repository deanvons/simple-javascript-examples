document
  .getElementById("simpleForm")
  .addEventListener("submit", handleFormSubmission);
const nameInput = document.getElementById("name");

function handleFormSubmission(event) {
  event.preventDefault(); // Prevents the default form submission behavior (page refresh), comment out and notice the page refreshes and the text input is reset.
  console.log(`Hello ${nameInput.value}`);
}
